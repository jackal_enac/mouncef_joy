cmake_minimum_required(VERSION 3.0.2)
project(mouncef_joy)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  message_generation
)

catkin_package(
  CATKIN_DEPENDS roscpp rospy std_msgs message_runtime
)


include_directories( include  ${catkin_INCLUDE_DIRS} )





add_executable(controler src/controler.cpp)
target_link_libraries(controler ${catkin_LIBRARIES})



add_executable(mouncef_driver src/mouncef_driver.cpp)
target_link_libraries(mouncef_driver ${catkin_LIBRARIES})

