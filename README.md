# Mouncef joy

A package to use ps4 game controller with ros 

## Requirements

This package requires you to already have a catkin workspace (catkin_ws) 

## Quick start

First change to the source space directory of the catkin workspace

```
$ cd ~/catkin_ws/src
```

clone the repository 

``` 
$ git clone https://gitlab.com/jackal_enac/mouncef_joy
```

Now you need to build the package in the catkin workspace: 

``` 
$ cd ~/catkin_ws
```

```
$ catkin_make
```

For a use with turtlesim : 
Run the following command to start the nodes

```
$ roslaunch mouncef_joy turtle_joy.launch 
```

For a use with jackal : 
Run the following command to start the nodes

```
$ roslaunch mouncef_joy jackal_joy.launch 
```

## New features
- Added safety button R1, the robot won't move until this button is pressed
- Added the ability to turn the robot by 90 degree to the left and to the right (sqaure for left and circle for right)
- Added the ability to move straight forward or backword in a straight line (X for forward and triangle for backward)
- Added the ability to move at a lower velocity, to do so, use the button R2 instead of R1