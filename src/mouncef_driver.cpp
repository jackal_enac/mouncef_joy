//Author : Mouncef CHADILI
//Contact : mouncef@chadili.com

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>


class Joystick
{
public:
  Joystick();

private:
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

  ros::NodeHandle nh_;

  int linear_, angular_, enable_button_, enable_turbo_button_, left_, right_, forward_, backward_  ;
  double l_scale_, a_scale_, time_now;
  ros::Publisher vel_pub_;
  ros::Subscriber joy_sub_;

};


Joystick::Joystick():
  linear_(1),
  angular_(2)
{

  nh_.param("axis_linear", linear_, linear_);
  nh_.param("axis_angular", angular_, angular_);
  nh_.param("scale_angular", a_scale_, a_scale_);
  nh_.param("scale_linear", l_scale_, l_scale_);
  nh_.param("enable_button", enable_button_, enable_button_);
  nh_.param("enable_turbo_button", enable_turbo_button_, enable_turbo_button_);
  nh_.param("left", left_, left_);
  nh_.param("right", right_, right_);
  nh_.param("forward", forward_, forward_);
  nh_.param("backward", backward_, backward_);


  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1);


  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &Joystick::joyCallback, this);

}

void Joystick::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;

  if (joy->buttons[enable_button_])
  {
    twist.angular.z = 0.2*a_scale_*joy->axes[angular_];
    twist.linear.x = 0.2*l_scale_*joy->axes[linear_];
  }

  if (joy->buttons[enable_turbo_button_])
  {

    // le 0.6 est un coefficient pour diminuer la vélocité
    twist.angular.z = 0.6*a_scale_*joy->axes[angular_];
    twist.linear.x = 0.6*l_scale_*joy->axes[linear_];
  }



  vel_pub_.publish(twist);





  if (joy->buttons[enable_turbo_button_])
  {

      // le 0.6 est un coefficient pour diminuer la vélocité
      // Pour tourner 90 degré à gauche
    if (joy->buttons[left_])
    {
      time_now = 0;
      ros::Rate rate(10);
      twist.angular.z = 0.926;

      while (time_now  < 17 )
      {
        vel_pub_.publish(twist);
        rate.sleep();
        time_now++; 
      }
    }


  // Pour tourner 90 degré à droite
      if (joy->buttons[right_])
    {
      time_now = 0;
      ros::Rate rate(10);
      twist.angular.z = -0.926;

      while (time_now  < 17 )
      {
        vel_pub_.publish(twist);
        rate.sleep();
        time_now++; 
      }
    }


  // Pour avancer d'une manière rectiligne
      if (joy->buttons[forward_])
    {
      twist.linear.x = 1.0;
      vel_pub_.publish(twist);
    }

  // Pour reculer d'une manière rectiligne
      if (joy->buttons[backward_])
    {
      twist.linear.x = -1.0;
      vel_pub_.publish(twist);
    }

    
  }

}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "teleop_turtle");
  Joystick teleop_turtle;

  ros::spin();
}